#!/bin/bash
#
# XNAT Vagrant provisioning script
# http://www.xnat.org
# Copyright (c) 2016, Washington University School of Medicine, all rights reserved.
# Released under the Simplified BSD license.
#

echo Now running the "provision.sh" provisioning script.

sourceScript() {
    test -f /vagrant/scripts/$1 && source /vagrant/scripts/$1 || source /vagrant-root/scripts/$1
}

# Now initialize the build environment from the config's vars.sh settings.
source /vagrant/.work/vars.sh

# look in config's scripts folder first, then try the multi root
sourceScript bin/macros
sourceScript defaults.sh

# set 'update_vm: false' to skip guest OS update (which can take a long time)
if [[ ${UPDATE_VM} != false ]]; then

    # Update the VM.
    ${INSTALL_CMD} update

    # Take an extra step with ubuntu: yum update does the whole thing, but for ubuntu we need to force upgrade to install everything without
    # requiring user interaction, especially grub upgrades.
    [[ ${ID} == "ubuntu" ]] && { ${INSTALL_CMD} -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" dist-upgrade; }

fi

# Install any additional specified packages
if [ -v INSTALL ]; then
    echo "Installing additional packages: ${INSTALL}"
    ${INSTALL_CMD} -y install ${INSTALL}
fi

# Update security stuff
sudo update-ca-certificates -f

# Create XNAT user
#  1. Create user group
#  2. Create user. Flags:
#       -g initial_group        User's initial login group
#       -G group[,...]          Supplementary groups of which user is a member
#       -d home_dir             User's login directory
#       -m                      Create home_dir if it does not exist
#       -s shell                User's login shell
echo ""

ROOT="$(dirname ${XNAT_HOME})"
sudo [ ! -d ${ROOT} ] && { mkdir -p ${ROOT}; }

grep -q -w docker /etc/group
[[ ${?} == 0 ]] && { XNAT_GROUPS="users,vagrant,docker"; } || { XNAT_GROUPS="users,vagrant"; }

if [[ ! -d ${XNAT_HOME} ]]; then
    echo "Creating XNAT user with the home directory ${XNAT_HOME}"
    sudo useradd -G ${XNAT_GROUPS} -d ${XNAT_HOME} -m -s /bin/bash ${XNAT_USER}
else
    echo "Creating XNAT user with the existing home directory ${XNAT_HOME}"
    sudo useradd -G ${XNAT_GROUPS} -d ${XNAT_HOME} -M -s /bin/bash ${XNAT_USER}
fi

# Create the various home folders and properties files if they don't already exist.
createUserHome

if [[ ! -z ${XNAT_PASS} ]]; then
    if [[ ${XNAT_PASS} == "default" ]]; then
        echo "${XNAT_USER}:${XNAT_USER}" | echo sudo chpasswd to default
    else
        echo "${XNAT_USER}:${XNAT_PASS}" | echo sudo chpasswd to set password
    fi
else
    echo XNAT_PASS not set or empty, leaving password unset, ssh public-private key access only.
fi

# Create the VM user's bash profile.
if [[ ! -e ${XNAT_HOME}/.bash_profile ]]; then
    echo ""
    echo "Creating XNAT user's bash profile"
    replaceTokens bash.profile | sudo tee ${XNAT_HOME}/.bash_profile
fi

# Set up ssh keys for VM user.
if [[ ! -e ${XNAT_HOME}/.ssh ]]; then
    echo ""
    echo "Copying vagrant ssh keys"
    sudo cp -R /home/vagrant/.ssh ${XNAT_HOME}
fi

# Now make anything non-XNAT_USER-y XNAT_USER-y.
setFolderOwner ${XNAT_USER} ${XNAT_HOME}

# Add VM user to list of NOPASSWD sudoers.
echo ""
echo "Adding XNAT user to list of NOPASSWD sudoers"
replaceTokens sudoers.d | sudo tee /etc/sudoers.d/${XNAT_USER}

## Set up Docker to listen for external connections
#echo ""
#echo "Creating Docker service configuration file"
#sudo mkdir /etc/systemd/system/docker.service.d
#replaceTokens docker.conf | sudo tee /etc/systemd/system/docker.service.d/docker.conf

# Setup Docker RemoteAPI
echo ""
echo "Opening access to port 2375 and docker.sock for Docker RemoteAPI"
echo "DOCKER_OPTS='-H tcp://0.0.0.0:2375 -H tcp://0.0.0.0:2376 -H unix:///var/run/docker.sock'" | sudo tee -a /etc/default/docker

echo ""
echo "Disabling the tomcat7 service during restart and provisioning."
toggleService tomcat7 off
