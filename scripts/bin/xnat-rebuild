#!/bin/bash

# This script is to be executed from inside the VM to rebuild XNAT.
# Gradle will be executed and dependencies downloaded in the VM.

# run `xnat-rebuild` from anywhere to rebuild XNAT from the current source with refreshed dependencies (default)
# run `xnat-rebuild pull` from anywhere to pull the latest source and rebuild XNAT with refreshed dependencies
# run `xnat-rebuild refresh:false` or `xnat-rebuild false` to rebuild WITHOUT refresh
# run `xnat-rebuild reset` (or `xnat-rebuild refresh reset`) from anywhere to rebuild with refresh and reset the db

OWD=`dirname $0`

REFRESH=$1
RESETDB=$2

# allow 'reset' to be passed as the only argument to reset the db (along with refreshing deps)
if [[ -z ${RESETDB} ]]; then
   [[ ${REFRESH} =~ false ]] && RESETDB=false
   [[ ${REFRESH} =~ reset ]] && RESETDB=reset && REFRESH=true
fi

[[ -e ${OWD}/vars.sh  ]] || { echo "vars.sh file is required for rebuild script"; exit -1; }

source ${OWD}/vars.sh
source ${OWD}/macros

echo Stopping Tomcat...
manageService tomcat7 stop

DATETIME=$(date +%Y.%m.%d-%H.%M.%S);

# create new folder for backup before build
BAKDIR="${XNAT_HOME}/backups/${DATETIME}"
mkdir -p ${BAKDIR}

# backup running webapp and war file
if [[ -e /var/lib/tomcat7/webapps/${CONTEXT} ]]; then
    echo ""
    echo "Backing up '${CONTEXT}' web app..."
    # back up webapp folder
    [[ -d /var/lib/tomcat7/webapps/${CONTEXT} ]] \
        && mv -fv /var/lib/tomcat7/webapps/${CONTEXT} ${BAKDIR}
    # back up war file
    [[ -f /var/lib/tomcat7/webapps/${CONTEXT}.war ]] \
        && mv -fv /var/lib/tomcat7/webapps/${CONTEXT}.war ${BAKDIR}
fi

echo ""
echo "Deleting '${CONTEXT}' web app and rebuilding from source..."

sudo rm -Rf /var/lib/tomcat7/webapps/${CONTEXT}*

if [[ -d ${DATA_ROOT}/src/${XNAT_DIR}  ]]; then

    cd ${DATA_ROOT}/src/${XNAT_DIR}

    if [[ $1 == pull ]]; then
        git pull
        PULL_STATUS=$?
        # if `git pull` fails, go back to previous dir and exit
        [[ ${PULL_STATUS} != 0 ]] \
            && echo "Git pull failed. Exiting." \
            && cd - \
            && exit ${PULL_STATUS}
    fi

    if [[ ! ${REFRESH} =~ (!|false) ]]; then
        echo Executing Gradle build with refresh...
        ./gradlew clean war --refresh-dependencies
    else
        echo Executing Gradle build...
        ./gradlew clean war # deployToTomcat # we'll manually copy the war file so we have it if we want to just redeploy
    fi

    # deploy first .war file found in /build/libs
    deployWar ${CONTEXT}

    cd -

fi

# Reset database?
[[ ${RESETDB} =~ (reset|true) ]] && resetDatabase ${XNAT_USER} ${PROJECT}

echo Restarting Tomcat...

startTomcat
monitorTomcatStartup
STATUS=$?

if [[ ${STATUS} == 0 ]]; then
    echo "==========================================================="
    echo "Rebuild completed successfully."
    echo "Your XNAT server will be available at: "
    echo "${SITE_URL}"
    echo "==========================================================="
else
    echo The application does not appear to have started properly. Status code: ${STATUS}
    echo The last lines in the log are:; tail -n 40 /var/log/tomcat7/catalina.out;
fi

exit ${STATUS}
