#!/usr/bin/env ruby
# convert YAML properties to vars for setup scripts

require 'yaml'

# pass the config directory as the first argument when executing this script
# ./yaml_vars.rb ../configs/foo
cfg_name = File.basename(Dir.getwd)
cfg_dir ||= ARGV[0] || cfg_name
cwd     ||= File.dirname(File.expand_path(cfg_name))

Dir.mkdir("#{cwd}/.work") unless File.exists?("#{cwd}/.work")

# load config settings
puts "Loading #{cwd}/config.yaml"
profile ||= YAML.load_file("#{cwd}/config.yaml")

# load common overrides from configs/global.yaml
global_config = "#{cwd}/../../configs/global.yaml"
if File.exists?(global_config)
    puts "Loading global overrides from #{global_config}..."
    global_yaml = YAML.load_file(global_config)
    global_yaml.each { |k, v|
        profile[k] = v
    }
end

# load local customizations
# (We really want to read these last, but we need to read the config file from here.)
local_path = "#{cwd}/local.yaml"
if File.exists?(local_path)
    puts "Loading local overrides from #{local_path}..."
    local = YAML.load_file(local_path)
    local.each { |k, v|
        profile[k] = v
    }
end

# set name to the folder name
profile['name'] ||= cfg_name != 'scripts' ? cfg_name : profile['name']

# setup some fallback defaults - some of these are for backwards compatibility
profile['verbose'] ||= false
profile['public_key'] ||= ''
profile['context'] ||= 'ROOT'
profile['project'] ||= profile['name']
profile['host'] ||= profile['name']
profile['data_root'] ||= "/data/#{profile['project']}"
profile['admin'] ||= "admin@#{profile['server']}"
profile['xnat_user'] ||= profile['project']
profile['xnat_pass'] ||= ''
profile['xnat_home'] ||= "#{profile['data_root']}/home"
profile['home_pkg'] ||= ''
profile['server'] ||= profile['vm_ip']
profile['revision'] ||= profile['xnat_rev'] ||= ''
profile['xnat_rev'] ||= profile['revision']
profile['pipeline_rev'] ||= profile['revision']
profile['branch'] ||= 'master'
profile['xnat_branch'] ||= profile['branch']
profile['pipeline_branch'] ||= profile['branch']

# this ugliness reconciles and conforms [xnat] and [xnat_dir]
profile['xnat'] = profile['xnat_dir'] ||= profile['xnat'] ||= 'xnat'

# reconciles and conforms [pipeline_inst] and [pipeline_dir]
profile['pipeline_inst'] = profile['pipeline_dir'] ||= profile['pipeline_inst'] ||= 'pipeline'

profile['config'] ||= cfg_dir ||= ''
profile['provision'] ||= ''
profile['build'] ||= ''

if profile['host'] == ''
    profile['host'] = profile['name']
end

if profile['server'] == ''
    profile['server'] = profile['vm_ip']
end

profile['protocol'] ||= 'http'

# build siteUrl setting from protocol and server
profile['site_url'] ||= "#{profile['protocol']}://#{profile['server']}#{profile['context'] == 'ROOT' ? '' : '/' + profile['context']}"

if profile['xnat_url'] || profile['xnat_repo']
    profile['xnat_src'] = profile['xnat_url'] ||= profile['xnat_repo']
end

if profile['pipeline_url'] || profile['pipeline_repo']
    profile['pipeline_src'] = profile['pipeline_url'] ||= profile['pipeline_repo']
end

shares = profile['shares'] ||= profile['shared'] ||= profile['share'] ||= ''
has_shares = shares && shares != 'false' && shares != ''

# If there's a setting for public key...
if profile['public_key']
    # Try to find that file
    if File.exists?(profile['public_key'])
        # If it exists, get it and just make sure it's not empty.
        keys = IO.readlines(profile['public_key'])
        if keys.length > 0
            puts "Retrieved public key from file #{profile['public_key']}"
            profile['public_key'] = keys[0].delete!("\n")
        else
            puts "Found the specified public key file at #{profile['public_key']}, but it appears to be empty."
            profile['public_key'] = ''
        end
    else
        puts "Public key file specified as #{profile['public_key']}, but I can't find that."
        profile['public_key'] = ''
    end
end

# write to a 'vars.yaml' file to serve as a Single Point of Truth after setup
puts "Initializing #{cwd}/vars.yaml"
File.open("#{cwd}/vars.yaml", 'wb') { |vars|
    vars.truncate(0)
    vars.puts("# vars.yaml\n")
    vars.puts("# DO NOT EDIT THE CONTENTS OF THIS FILE.\n")
    vars.puts("# IT IS AUTOMATICALLY GENERATED ON SETUP.\n")
    profile.each { |k, v|
        if has_shares && %w[shares shared share].include?(k)
            if profile['verbose']
                puts " * #{k}:\n"
            end
            vars.puts "#{k}:\n"
            v.each { |share, share_to|
                if profile['verbose']
                    puts " *   \"#{share}\":\n"
                end
                vars.puts "  \"#{share}\":\n"
                share_to.each { |param|
                    if profile['verbose']
                        puts " *     - #{param}"
                    end
                    vars.puts "    - #{param}"
                }
            }
        else
            # empty values get changed to ""
            if profile['verbose']
                puts " * #{k}: " + (v == '' ? '""' : "#{v}")
            end
            vars.puts "#{k}: " + (v == '' ? '""' : "#{v}")
        end
    }
}

puts "Initializing #{cwd}/.work/vars.sh"
File.open("#{cwd}/.work/vars.sh", 'wb') { |vars|
    vars.truncate(0)
    vars.puts("#!/bin/bash\n")
    profile.each { |k, v|
        if has_shares && %w[shares shared share].include?(k)
            if profile['verbose']
                puts " * #{k.upcase}='#{v}'"
            end
            vars.puts "#{k.upcase}='#{v}'"
        else
            # empty values get changed to ""
            if profile['verbose']
                puts " * #{k.upcase}=" + (v == '' ? '""' : "\"#{v}\"")
            end
            vars.puts "#{k.upcase}=" + (v == '' ? '""' : "\"#{v}\"")
        end
    }
}

puts "Initializing #{cwd}/.work/vars.sed"
File.open("#{cwd}/.work/vars.sed", 'wb') { |vars|
    vars.truncate(0)
    profile.each { |k, v|
        # exclude shares setting
        unless %w[shares shared share].include?(k)
            _v = "#{v}"
            if profile['verbose']
                puts " * s/@#{k.upcase}@/#{_v.gsub('/', "\\/")}/g"
            end
            vars.puts "s/@#{k.upcase}@/#{_v.gsub('/', "\\/")}/g"
        end
    }
}
